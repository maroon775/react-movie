var changed, gulp, handleErrors, pngquant, spritesmith;
gulp = require('gulp');
changed = require('gulp-changed');
spritesmith = require('gulp.spritesmith');
handleErrors = require('../util/handleErrors');

gulp.task('imagesSprites', function(){
	var spriteData;
	spriteData = gulp.src('./src/images/sprite/*.*')
		.pipe(spritesmith({
			algorithm  :'top-down',
			imgName    :'sprite.png',
			cssName    :'sprite.less',
			imgPath    :'../i/bg/sprite.png',
			imgOpts    :{
				quality:100
			},
			cssTemplate:'less.template.spritesmith'
		})).on('error', handleErrors);
	spriteData.img.pipe(gulp.dest('./src/i/bg/'));
	return spriteData.css.pipe(gulp.dest('./src/styles/lib/icons'));
});
