var babelify, browserify, browserifyCss, buffer, bundleLogger, gulp,
    handleErrors, rename, source, uglify, watchify;
gulp = require('gulp');
browserify = require('browserify');
babelify = require('babelify');
watchify = require('watchify');
uglify = require('gulp-uglify');
source = require('vinyl-source-stream');
buffer = require('vinyl-buffer');
rename = require('gulp-rename');
bundleLogger = require('../util/bundleLogger');
handleErrors = require('../util/handleErrors');


gulp.task('react', function(){
	var clientBundler, rebundle, vendorBundler;
	clientBundler = browserify('./src/app/index.jsx', {
		extensions:['.jsx', '.js'],
		debug     :false,
		cache     :{},
	}).transform('babelify', {
		presets:['es2015', 'react']
	});
	
	rebundle = function(){
		bundleLogger.start('bundle.js');
		return clientBundler
			.bundle()
			.on('error', handleErrors)
			.pipe(source('bundle.js'))
			.pipe(gulp.dest('./build/js'))
			/*.pipe(buffer())
			.pipe(uglify())
			.pipe(rename({
				suffix:'.min'
			}))
			.pipe(gulp.dest('./build/js'))*/.on('end', function(){
				return bundleLogger.end('bundle.js');
			});
	};
	if(global.isWatching)
	{
		clientBundler = watchify(clientBundler, {
			delay:200,
			poll :500
		});
		clientBundler.on('update', rebundle);
	}
	rebundle();
});
