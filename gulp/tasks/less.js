var autoprefixer, csso, gulp, handleErrors, less, path, rename, sourcemaps;
gulp         = require('gulp');
less         = require('gulp-less');
rename       = require('gulp-rename');
autoprefixer = require('gulp-autoprefixer');
path         = require('path');
sourcemaps   = require('gulp-sourcemaps');
csso         = require('gulp-csso');
handleErrors = require('../util/handleErrors');

gulp.task('less', function ()
{
	return gulp.src('./src/styles/main.less')
		.pipe(sourcemaps.init())
		.pipe(less({
			paths: [path.join(__dirname, 'less')]
		}))
		.on('error', handleErrors)
		.pipe(autoprefixer({
			cascade : false,
			browsers: ['last 2 versions', 'Safari >= 8']
		}))
		.pipe(sourcemaps.write())
		.pipe(rename('style.css'))
		.pipe(gulp.dest('./build/css'))
		.pipe(csso({
			restructure: false,
			sourceMap  : false,
			debug      : true
		}))
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(gulp.dest('./build/css'));
});
