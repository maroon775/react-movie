var gulp;

gulp = require('gulp');

gulp.task('watch', ['browserSync'], function ()
{
	gulp.watch('src/js/**/*.js', ['js']);
	gulp.watch('src/html/**/*.html', ['html']);
	gulp.watch('src/styles/**/*.less', ['less']);
	gulp.watch([
		'./src/images/**/*.*',
		'!./src/images/sprite/**/*.*'
	], ['images']);
	gulp.watch(['./src/images/sprite/**/*.*'], ['imagesSprites']);
	return true;
});

gulp.task('setWatch', function ()
{
	return global.isWatching = true;
});
