var gulp, handleErrors, uglify;
gulp         = require('gulp');
uglify       = require('gulp-uglify');
handleErrors = require('../util/handleErrors');

gulp.task('bower', function ()
{
	var arJs;
	arJs = [];
	arJs.push('./bower_components/jquery/dist/jquery.min.js');
	
	return gulp.src(arJs).pipe(gulp.dest('./src/js/'));
});
