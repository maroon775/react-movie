var browserSync, gulp, reload, rigger;
gulp        = require('gulp');
rigger      = require('gulp-rigger');
browserSync = require('browser-sync');
reload      = browserSync.reload;

gulp.task('html', function ()
{
	return gulp.src('./src/html/*.html').pipe(rigger()).pipe(gulp.dest('./build')).pipe(reload({
		stream: true
	}));
});
