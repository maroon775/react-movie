import React from 'react';
import ReactDOM from 'react-dom';
import {composeWithDevTools} from 'redux-devtools-extension';
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {Provider} from 'react-redux';
import App from './App';

import reducer from './reducers/index';

var store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)));


ReactDOM.render((
	<Provider store={store}>
		<App/>
	</Provider>
), document.getElementById('applicationRoot'));
