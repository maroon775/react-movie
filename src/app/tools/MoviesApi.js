import ajax from '@fdaciuk/ajax';

const API_PATH = 'https://api.themoviedb.org/3';
const IMAGES_BASE_URL = 'https://image.tmdb.org/t/p/';

class MoviesApi {
	constructor(params)
	{
		this.api_key = params.api_key || '';
		this.language = params.language || 'en-Us';
		this.api = ajax({baseUrl:API_PATH});
	}
	
	get(path, params)
	{
		let _defaults = {language:this.language};
		
		let _params = Object.assign({}, _defaults, params, {api_key:this.api_key});
		return this.api.get(path, _params);
	}
}

export const getPosterImageUrls = (path, size) =>{
	let sizes = {
		"w92"     :IMAGES_BASE_URL + "w92" + path,
		"w154"    :IMAGES_BASE_URL + "w154" + path,
		"w185"    :IMAGES_BASE_URL + "w185" + path,
		"w342"    :IMAGES_BASE_URL + "w342" + path,
		"w500"    :IMAGES_BASE_URL + "w500" + path,
		"w780"    :IMAGES_BASE_URL + "w780" + path,
		"original":IMAGES_BASE_URL + "original" + path
	}
	
	return size && size in sizes ? sizes[size] : sizes;
}

export const getBackdropImageUrls = (path, size) =>{
	let sizes = {
		"w300"    :IMAGES_BASE_URL + "w300" + path,
		"w780"    :IMAGES_BASE_URL + "w780" + path,
		"w1280"   :IMAGES_BASE_URL + "w1280" + path,
		"original":IMAGES_BASE_URL + "original" + path
	};
	
	return size && size in sizes ? sizes[size] : sizes;
}

export const getImageUrl = (path, size) =>{
	return IMAGES_BASE_URL + (size.toString()) + path;
}
export default MoviesApi;

MoviesApi.getPosterImageUrls = getPosterImageUrls;
MoviesApi.getBackdropImageUrls = getBackdropImageUrls;
MoviesApi.getImageUrl = getImageUrl;
