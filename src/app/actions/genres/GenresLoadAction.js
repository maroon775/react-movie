import * as config from "../../config";
import MoviesApi from '../../tools/MoviesApi';
import store from 'store';

export const TYPE_GENRES_LOADED = 'GENRES_LOADED';
export const TYPE_GENRES_GET = 'GENRES_GET';
export const GenresLoadAction = () => dispatch =>{
	
	let genres = store.get('cached_genres');
	
	if(genres && genres.data && genres.expire >= Date.now())
	{
		return dispatch({
			type   :TYPE_GENRES_GET,
			payload:genres.data
		})
	}
	else
	{
		let Api = new MoviesApi({api_key:config.themoviedb.api_key});
		Api.get('/genre/movie/list').then(function(data){
			console.log('Api get /genre/movie/list', data);
			
			let genres = {};
			data.genres && data.genres.forEach((item) =>{
				genres[item.id] = item.name
			});
			
			store.set('cached_genres', {
				expire:Date.now() + (3600 * 1000),
				data  :genres
			});
			
			dispatch({
				type   :TYPE_GENRES_LOADED,
				payload:genres
			});
		});
	}
};
