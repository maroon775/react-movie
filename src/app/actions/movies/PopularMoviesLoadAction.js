import * as config from "../../config";
import MoviesApi from '../../tools/MoviesApi';

export const TYPE_POPULAR_MOVIES_LOADED = 'MOVIES_POPULAR_LOADED';
export const PopularMoviesLoadAction = (page = 1) => dispatch =>{
	
	let Api = new MoviesApi({api_key:config.themoviedb.api_key});
	
	let _page = parseInt(page) >= 1 ? parseInt(page) : 1;
	
	Api.get('/movie/popular', {page:_page}).then(function(data){
		console.log('Api get /movie/popular', data);
		setTimeout(() =>{
			dispatch({
				type   :TYPE_POPULAR_MOVIES_LOADED,
				payload:data
			})
		}, 3000);
	});
};
