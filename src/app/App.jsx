import React from "react";
import {connect} from "react-redux";

import {getPosterImageUrls} from './tools/MoviesApi';
import {PopularMoviesLoadAction} from './actions/movies/PopularMoviesLoadAction';
import {PopularMoviesLoadMoreAction} from './actions/movies/PopularMoviesLoadMoreAction';
import {GenresLoadAction} from './actions/genres/GenresLoadAction';

import {Container, Header, Button, Segment, Divider} from 'semantic-ui-react'
import MoviesGrid from './components/MoviesGrid';
import LoadMoreButton from './components/LoadMoreButton';

class App extends React.Component {
	constructor(props)
	{
		super(props);
		
		this.onButtonClick = this.onButtonClick.bind(this);
		console.log('constructor', this.props);
		this.props.GenresLoad();
		this.props.PopularMoviesLoad();
	}
	
	render()
	{
		let movies = this.props.moviesPopular.results && this.props.moviesPopular.results.map((movie, index) =>
			({
				"link"       :'#',
				"imagePath"  :getPosterImageUrls(movie.poster_path, 'w500'),
				"title"      :movie.title,
				"secondTitle":movie.original_title,
				"description":movie.overview,
				"rating"     :movie.vote_average,
				"genres"     :movie.genre_ids && movie.genre_ids.slice(0, 4).map((item) => this.props.genres[item])
			}));
		let dataExist = this.props.moviesPopular.results && this.props.moviesPopular.results.length > 0;
		return (
			<Container fluid>
				<Segment inverted style={{padding:'2em 0em'}} vertical>
					<Header as="h3" textAlign="center">Popular movies</Header>
				</Segment>
				<Container style={{padding:'2em 0em'}} className="four column doubling centered grid">
					<MoviesGrid data={movies}/>
					
					{dataExist && (<Container textAlign="center">
						<Divider horizontal><LoadMoreButton onClick={this.onButtonClick}/></Divider>
					</Container>)}
				</Container>
			</Container>);
	}
	
	onButtonClick(e)
	{
		console.log(this.props, this.state);
		let page = 0;
		
		if(this.props.moviesPopular && this.props.moviesPopular.page)
		{
			page = parseInt(this.props.moviesPopular.page) || page;
		}
		this.props.LoadMoreMovies(page);
		return false;
	}
}

export default connect(
	state => ({
		moviesPopular:state.PopularMoviesLoadReducer,
		genres       :state.GenresLoadReducer
	}),
	dispatch => ({
		GenresLoad       :() =>{
			dispatch(GenresLoadAction());
		},
		PopularMoviesLoad:() =>{
			dispatch(PopularMoviesLoadAction(1));
		},
		LoadMoreMovies   :(currentPage) =>{
			const nextPage = (parseInt(currentPage) || 0) + 1;
			dispatch(PopularMoviesLoadMoreAction(nextPage));
		}
	})
)(App);
