import React from "react";
import PropTypes from 'prop-types';
import MovieItemCard from './MovieItemCard';
import {Grid, Dimmer,Loader} from 'semantic-ui-react';

class MoviesGrid extends React.Component {
	render()
	{
		let movies;
		if(this.props.data)
		{
			movies = this.props.data.map((props, index) => (<Grid.Column key={index}>{React.cloneElement(<MovieItemCard />, props)}</Grid.Column>));
		}
		else
		{
			return (<Dimmer active><Loader size='large'>Loading</Loader></Dimmer>);
		}
		return (movies);
	}
}

MoviesGrid.propsTypes = {
	data:PropTypes.array
};

export default MoviesGrid;
