import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import {Card, Icon, Image} from 'semantic-ui-react';
import '../tools/textTruncate';
import textTruncate from "../tools/textTruncate";

class MovieItemCard extends React.Component {
	render()
	{
		return (
			<Card className="centered fluid" href={this.props.link}>
				{/*<Link to={this.props.link}></Link>*/}
				<Image src={this.props.imagePath} label={{ as: 'span', color: 'red', content: this.props.genres.join(', '), ribbon: true }}/>
				<Card.Content>
					<Card.Header>
						{this.props.title}
					</Card.Header>
					<Card.Meta>
						{this.props.secondTitle}
					</Card.Meta>
					<Card.Description>
						{textTruncate(this.props.description, 150, '...')}
					</Card.Description>
				</Card.Content>
				<Card.Content extra>
					<Icon name='bar chart'/>&nbsp;{this.props.rating}
				</Card.Content>
			
			</Card>);
	}
}

export default MovieItemCard
