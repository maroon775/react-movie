import React from "react";
import {Button} from 'semantic-ui-react';

class LoadMoreButton extends React.Component {
	constructor(props)
	{
		super(props);
		
		
		this.onButtonClick = this.onButtonClick.bind(this);
	}
	
	render()
	{
		let props = Object.assign({}, this.props, {
			onClick:this.onButtonClick
		})
		return (<Button {...props}>Load More</Button>);
	}
	
	onButtonClick(e)
	{
		this.props.onClick(e)
	}
}

export default LoadMoreButton;
