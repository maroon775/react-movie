import {TYPE_POPULAR_MOVIES_LOADED} from '../../actions/movies/PopularMoviesLoadAction';
import {TYPE_POPULAR_MOVIES_MORE_LOADED} from '../../actions/movies/PopularMoviesLoadMoreAction';

export default function PopularMoviesLoadReducer(state = [], action){
	switch(action.type)
	{
		case TYPE_POPULAR_MOVIES_LOADED:
			return {
				page   :action.payload.page,
				results:action.payload.results
			}
			break;
		case TYPE_POPULAR_MOVIES_MORE_LOADED:
			console.log(TYPE_POPULAR_MOVIES_MORE_LOADED,state);
			
			return {
				page   :action.payload.page,
				results:state.results.concat(action.payload.results)
			}
			break;
		default:
			return state;
	}
	
}
