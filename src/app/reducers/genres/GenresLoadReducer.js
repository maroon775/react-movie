import {TYPE_GENRES_LOADED, TYPE_GENRES_GET} from '../../actions/genres/GenresLoadAction';
import store from 'store';

var genres = store.get('cached_genres');
var genresInitialValues = genres && genres.data ? genres.data : {};

export default function GenresLoadReducer(state = genresInitialValues, action){
	console.log('genresInitialValues', state);
	switch(action.type)
	{
		case TYPE_GENRES_LOADED:
		case TYPE_GENRES_GET:
			return action.payload;
			break;
		default:
			return state;
	}
	
}
