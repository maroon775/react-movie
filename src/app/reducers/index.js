import {combineReducers} from 'redux';
import PopularMoviesLoadReducer from './movies/PopularMoviesLoadReducer';
import GenresLoadReducer from './genres/GenresLoadReducer';

export default combineReducers({
	PopularMoviesLoadReducer,
	GenresLoadReducer
});
